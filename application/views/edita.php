<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>G.I.F</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/reset.css'); ?>">

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/mystyle.css'); ?>">
</head>
<body>
	<div class="faixa"></div>

	<section class="container_funcionarios">
		<div class="container">
		<a class="btnlogout" href="<?= base_url('index.php/logout'); ?>">SAIR</a>
			<div class="c_funcionarios">
				<h1 class="titulo_page">Editando o(a) funcionário(a) <?= $dados->nome; ?></h1>
				<a href="javascript:window.history.back()" style="font-family:Arial;font-size:17px;text-decoration:none;color:green;display:block;">< Voltar</a>

					<img style="max-width:140px;margin:15px auto;display:block;" src="<?= base_url('assets/uploads').'/'.$dados->foto; ?>">
				<div class="c_form_add_funncionarios">
					<form action="<?= base_url('index.php').'/editar/'.$dados->id; ?>" method="post" class="form_add_funcionarios"  enctype="multipart/form-data">
						<label class="label_foto" for="foto">Foto</label><span id="nome_foto"></span>
						<input style="visibility:hidden;" type="file" id="foto" class="form_add_file" value="" name="userfile">

						<label for="nome">Nome</label>
						<input type="text" id="nome" class="form_add_input" value="<?= $dados->nome; ?>" name="nome" placeholder="Nome" required>
						
						<label for="email">E-mail</label>
						<input type="text" id="email" class="form_add_input" value="<?= $dados->email; ?>" name="email" placeholder="E-mail" required>
						
						<label for="setor">Setor</label>
						<input type="text" id="setor" class="form_add_input" value="<?= $dados->setor; ?>" name="setor" placeholder="Setor" required>
						
						<label for="cargo">Cargo</label>
						<input type="text" id="cargo" class="form_add_input" value="<?= $dados->cargo; ?>" name="cargo" placeholder="Cargo" required>

						<button class="form_add_button">Atualizar Registro</button>
					</form>
				</div>
			</div>
		</div>
	</section>

	<script type="text/javascript" src="<?= base_url('assets/js/jquery.js'); ?>"></script>
	<script type="text/javascript" src="<?= base_url('assets/js/myscript.js'); ?>"></script>
</body>
</html>