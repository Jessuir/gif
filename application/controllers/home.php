<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('crud');
	}

	public function index()
	{
		if (!$this->session->userdata('is_logged_in')) redirect(base_url(), 'refresh');
		$dados['dados'] = $this->crud->lista('funcionarios');
		
		$this->load->view('home', $dados);
	}

	public function deleta_funcionario($id)
	{
		if (!$this->session->userdata('is_logged_in')) redirect(base_url(), 'refresh');
		$this->crud->deleta('funcionarios', $id);
		
		redirect('home', 'refresh');
	}

}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */