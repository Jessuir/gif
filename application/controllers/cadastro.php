<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Cadastro extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('crud');
	}

	public function index()
	{
		if (!$this->session->userdata('is_logged_in')) redirect(base_url(), 'refresh');
		$dados['dados'] = $this->crud->lista('funcionarios');
		
		$this->load->view('cadastro', $dados);
	}

	public function cadastra_func()
	{
		if (!$this->session->userdata('is_logged_in')) redirect(base_url(), 'refresh');
		$config['upload_path'] = './assets/uploads/';
      	$config['allowed_types'] = 'jpg|jpeg|gif|png|';    	
		$config['max_size']	= 0;
		$config['encrypt_name'] = TRUE;
		$config['remove_spaces'] = TRUE;
		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload('userfile'))
		{
			$error = array('error' => $this->upload->display_errors());
			$dados_form = array(
				'nome' => $this->input->post('nome'),
				'email' => $this->input->post('email'),
				'setor' => $this->input->post('setor'),
				'cargo' => $this->input->post('cargo'),
				'foto' => 'sem_foto.png',
			);

			$this->crud->cadastra('funcionarios', $dados_form);
		}
		else
		{
			$data = array('upload_data' => $this->upload->data());
			$dados_form = array(
				'nome' => $this->input->post('nome'),
				'email' => $this->input->post('email'),
				'setor' => $this->input->post('setor'),
				'cargo' => $this->input->post('cargo'),
				'foto' => $this->upload->file_name,
			);

			$this->crud->cadastra('funcionarios', $dados_form);

		}
			redirect('home', 'refresh');
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */