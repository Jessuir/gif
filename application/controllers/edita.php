<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Edita extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('crud');
	}

	public function index($id)
	{
		if (!$this->session->userdata('is_logged_in')) redirect(base_url(), 'refresh');
		$dados['dados'] = $this->crud->lista_edita('funcionarios', $id);
		
		$this->load->view('edita', $dados);
	}

	public function edita_func($id)
	{
		if (!$this->session->userdata('is_logged_in')) redirect(base_url(), 'refresh');
		if(!$_FILES['userfile']['tmp_name']){
			
			$dados_form = array(
				'nome' => $this->input->post('nome'),
				'email' => $this->input->post('email'),
				'setor' => $this->input->post('setor'),
				'cargo' => $this->input->post('cargo'),
			);

			$this->crud->edita('funcionarios', $id, $dados_form);
		}
		else
		{

			$config['upload_path'] = './assets/uploads/';
	      	$config['allowed_types'] = 'jpg|jpeg|gif|png|';    	
			$config['max_size']	= 0;
			$config['encrypt_name'] = TRUE;
			$config['remove_spaces'] = TRUE;
			$this->load->library('upload', $config);

			if ( ! $this->upload->do_upload('userfile'))
			{
				$error = array('error' => $this->upload->display_errors());

			}
			else
			{
				$data = array('upload_data' => $this->upload->data());
				$dados_form = array(
					'nome' => $this->input->post('nome'),
					'email' => $this->input->post('email'),
					'setor' => $this->input->post('setor'),
					'cargo' => $this->input->post('cargo'),
					'foto' => $this->upload->file_name,
				);

				$this->crud->edita('funcionarios', $id, $dados_form);
			}


		}

			redirect('home', 'refresh');
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */