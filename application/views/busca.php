<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>G.I.F</title>

	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/reset.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/mystyle.css'); ?>">
</head>
<body>
	<div class="faixa"></div>

	<section class="container_funcionarios">
		<div class="container">
		<a class="btnlogout" href="<?= base_url('index.php/logout'); ?>">SAIR</a>
			<div class="c_funcionarios">
				<h1 class="titulo_page">Busca por funcionários cadastrados</h1>
				<div class="form_busca">
					<form class="form_busca_form" method="post" action="busca">
						<label style="font-family:Arial;color:green;">Busque por qualquer campo: </label>
						<input type="text" class="form_busca_input" name="busca" placeholder="Id,Nome,E-mail,Setor,Cargo">	
					</form>
				</div>
				<a href="cadastro" class="btnadd_func">Adicionar Usuários</a>

				<div class="conteudo_funcionarios">
					<div class="titulos_ordem">
						<div class="titulo_funcionario"><p>Foto</p></div>
						<div class="titulo_funcionario"><p>Id</p></div>
						<div class="titulo_funcionario"><p>Nome</p></div>
						<div class="titulo_funcionario"><p>E-mail</p></div>
						<div class="titulo_funcionario"><p>Setor</p></div>
						<div class="titulo_funcionario"><p>Cargo</p></div>
					</div>


					<?php 
					if (isset($dados[0]['msg']))
					{
						echo '<div class="c_msg_void"><h1 class="msg_void">'.$dados[0]['msg'].'</h1></div>';
					}
					else{
					 for ($i=0; $i < count($dados); $i++) {
					?>
					<div class="lista_funcionarios">
						<div class="btnoptions">
							<a href="edita/<?= $dados[$i]['id']; ?>">editar</a>
							<a href="deleta/<?= $dados[$i]['id']; ?>" onclick="return confirm('Tem certeza que deseja excluir <?= $dados[$i]['nome']; ?> do registro ?')">excluir</a>
						</div>
						<div class="info_func-foto_funcionario" style="background-image:url(<?= base_url('assets/uploads/').'/'.$dados[$i]['foto']; ?>);"></div>
						<div class="info_func-id_funcionario"><p><?= $dados[$i]['id']; ?></p></div>
						<div class="info_func-nome_funcionario"><p><?= $dados[$i]['nome']; ?></p></div>
						<div class="info_func-email_funcionario"><p><?= $dados[$i]['email']; ?></p></div>
						<div class="info_func-setor_funcionario"><p><?= $dados[$i]['setor']; ?></p></div>
						<div class="info_func-cargo_funcionario"><p><?= $dados[$i]['cargo']; ?></p></div>
					</div>
					<?php } }?>
				</div>
			</div>
		</div>
	</section>
</body>
</html>