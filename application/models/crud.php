<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Crud extends CI_Model {
	
	public function cadastra($tabela = NULL, $dados_form = NULL)
	{
		$this->db->insert($tabela, $dados_form);
	}

	public function lista($tabela = NULL)
	{
		$this->db->order_by('id', 'DESC');
		$query = $this->db->get($tabela);

		$array = array();

		foreach($query->result() as $row)
		{
			array_push($array, array(
				"id"		=>	$row->id,
				"nome"		=>	$row->nome,
				"email"		=>	$row->email,
				"setor"		=>	$row->setor,
				"cargo"		=>	$row->cargo,
				"foto"		=>	$row->foto
			));
		}

		return $array;
	}

	public function lista_edita($tabela = NULL, $id_funcionario = NULL)
	{
		$this->db->where('id', $id_funcionario);
		$query = $this->db->get($tabela);

		return $query->row();
	}

	public function edita($tabela = NULL, $id_funcionario = NULL, $dados_form = NULL)
	{
		$this->db->where('id', $id_funcionario);
		$this->db->update($tabela, $dados_form);
	}

	public function deleta($tabela = NULL, $id_funcionario = NULL)
	{
		$this->db->where('id', $id_funcionario);
		$this->db->delete($tabela);
	}

	public function busca($tabela = NULL, $dado_busca = NULL)
	{
		$this->db->like('id', $dado_busca);
		$this->db->or_like('nome', $dado_busca);
		$this->db->or_like('email', $dado_busca);
		$this->db->or_like('setor', $dado_busca);
		$this->db->or_like('cargo', $dado_busca);
		$query = $this->db->get($tabela);

		$array = array();

		if ($query->result())
		{
			foreach($query->result() as $row)
			{
				array_push($array, array(
					"id"		=>	$row->id,
					"nome"		=>	$row->nome,
					"email"		=>	$row->email,
					"setor"		=>	$row->setor,
					"cargo"		=>	$row->cargo,
					"foto"		=>	$row->foto
				));
			}
			return $array;
		}
		else
		{
			array_push($array, array(
					"msg"		=>	"Nenhum funcionário encontrado."."<br />"."Por favor tente com outros dados.",
				));

			return $array;
		}


	}

	public function checa_login($tabela = NULL, $dadosUsers = NULL){
		
		$this->db->where('user', $dadosUsers['user']);
		$this->db->where('pass', $dadosUsers['pass']);
		$query = $this->db->get($tabela);
		if($query->num_rows == 1)
		{
			return TRUE;
		}
		else
		{
			return $this->session->set_flashdata('login_fail', 'Usuário ou Senha incorreto!');;
		}
	}

}