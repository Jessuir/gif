-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Máquina: localhost
-- Data de Criação: 05-Abr-2015 às 03:48
-- Versão do servidor: 5.5.41-0ubuntu0.14.04.1
-- versão do PHP: 5.5.9-1ubuntu4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de Dados: `dev_gif`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_funcionarios`
--

CREATE TABLE IF NOT EXISTS `tb_funcionarios` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `setor` varchar(255) DEFAULT NULL,
  `cargo` varchar(255) DEFAULT NULL,
  `foto` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=24 ;

--
-- Extraindo dados da tabela `tb_funcionarios`
--

INSERT INTO `tb_funcionarios` (`id`, `nome`, `email`, `setor`, `cargo`, `foto`) VALUES
(1, 'Pedro', 'pedro@dj.emp.br', 'TI', 'Gerente', 'sem_foto.png'),
(2, 'João', 'joao@dj.emp.br', 'TI', 'Programador', 'sem_foto.png'),
(3, 'Flavio', 'flavio@dj.emp.br', 'TI', 'Estagiário', 'sem_foto.png'),
(4, 'Maria', 'maria@dj.emp.br', 'Administrativo', 'Secretária', 'sem_foto.png'),
(5, 'Amanda', 'amanda@dj.emp.br', 'Administrativo', 'Estagiária', 'sem_foto.png'),
(6, 'Manoel', 'manoel@dj.emp.br', 'Financeiro', 'Contador', 'sem_foto.png'),
(7, 'Laura', 'laura@dj.emp.br', 'RH', 'Psicóloga', 'sem_foto.png'),
(8, 'Debora', 'debora@dj.emp.br', 'RH', 'Gerente', 'sem_foto.png'),
(9, 'Bruno', 'bruno@dj.emp.br', 'Diretoria', 'CEO', 'sem_foto.png'),
(10, 'Rodrigo', 'rodrigo@dj.emp.br', 'Diretoria', 'CFO', 'sem_foto.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `tb_users`
--

CREATE TABLE IF NOT EXISTS `tb_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user` varchar(255) NOT NULL,
  `pass` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Extraindo dados da tabela `tb_users`
--

INSERT INTO `tb_users` (`id`, `user`, `pass`) VALUES
(1, 'admin', '87d6ae1d2e9d885d34d0457367be856b5beb728c');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
