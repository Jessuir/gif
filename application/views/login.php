<!DOCTYPE html>
<html lang="pt-br">
<head>
	<meta charset="utf-8">
	<title>G.I.F</title>
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/reset.css'); ?>">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/mystyle.css'); ?>">
</head>
<body>
	<div class="container">
		<div class="c_form_login">
		<h1 class="titulo_login"><?php echo $titulo; ?></h1>
		<?php
			if ($this->session->flashdata('login_fail')) {
			 	echo '<span class="login_fail">'.$this->session->flashdata('login_fail').'</span>'; 
			}
		?>
			<form class="form_login" method="post" action="<?= base_url('index.php/logar'); ?>">
				<input class="form_login_input" type="text" name="user" placeholder="Usuário" required>
				<input class="form_login_input" type="password" name="pass" placeholder="Senha" required>
				<button class="form_login_button">Entrar</button>
			</form>	
		</div>	
	</div>
</body>
</html>