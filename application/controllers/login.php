<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('crud');
	}

	public function index()
	{
		// $dadosUsuario = array(
		// 	'user' 	=> 'admin', 
		// 	'pass' => do_hash('admfuncionarios31@'), 
		// );

		// $this->crud->cadastra('users', $dadosUsuario);

		if ($this->session->userdata('is_logged_in')) redirect('home', 'refresh');
		$dados = array('titulo' => 'Gerenciamento de Informações de Funcionários [G.I.F]');
		$this->load->view('login', $dados);
	}

	public function logar()
	{
		if ($this->session->userdata('is_logged_in')) redirect('home', 'refresh');

		$dadosUsuario = array(
			'user' 	=> $this->input->post('user'), 
			'pass' => do_hash($this->input->post('pass')), 
		);
		
		
		$query = $this->crud->checa_login('users', $dadosUsuario);

		
		if($query)
		{
			$data = array(
				'is_logged_in' => true
			);
			$this->session->set_userdata($data);
			redirect('home', 'refresh');
		}else // user ou pass incorreto
		{
			redirect(base_url(), 'refresh');
		}

	}

	public function logout(){
	
		if($this->session->userdata('is_logged_in')){
			$this->session->sess_destroy();
			redirect(base_url(), 'refresh');
		}else{
			redirect(base_url(), 'refresh');
			
		}
		
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */