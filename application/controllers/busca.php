<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Busca extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->model('crud');
	}

	public function index()
	{
		if (!$this->session->userdata('is_logged_in')) redirect(base_url(), 'refresh');
		$dados['dados'] = $this->crud->busca('funcionarios', $this->input->post('busca'));
		
		$this->load->view('busca', $dados);
	}


}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */